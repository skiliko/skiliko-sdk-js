const store = {
  settings: JSON.stringify({
    PlatformToken: 'La96unwYSVKnOPxnfYlk-A',
  }),
};

global.localStorage = {
  getItem: (key) => store[key],
  setItem: (key, value) => store[key] = value,
};
