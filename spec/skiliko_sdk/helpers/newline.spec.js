describe('newLine', () => {
  const newLine = require('../../../lib/helpers/newline').Newline;
  const text = "hello\ntest\r\nnew\r\line\n";
  const result = "hello<br />test<br />new<br />line<br />";

  it('should replace new lines by <br>', () => {
    expect(newLine(text)).toBe(result);
  });
});
