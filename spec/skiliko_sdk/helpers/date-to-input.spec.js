describe('dateToInput', () => {
  const dateToInput = require('../../../lib/helpers/date-to-input').DateToInput;
  const year = 2016;
  const month = 10; // October
  const day = 12;
  const date = `${year}-${month}-${day}T00:00:00.000Z`;

  it('should return the correct format', () => {
    expect(dateToInput(date)).toBe(`${year}-${month}-${day}`);
  });
});
