describe('dateFromJson', () => {
  const dateFromJson = require('../../../lib/helpers/date-from-json').DateFromJson;
  const year = 2016;
  const month = 10; // October
  const day = 12;

  it('should match google analytics date', () => {
    const googleDate = [year, month, day].join('');
    const res = dateFromJson(googleDate);
    expect(res.getDate()).toBe(day);
    expect(res.getMonth() + 1).toBe(month);
    expect(res.getFullYear()).toBe(year);
  });

  it('should return nothing if no input', () => {
    expect(dateFromJson(null)).toBe(null);
    expect(dateFromJson('')).toBe(null);
    expect(dateFromJson(undefined)).toBe(null);
  });

  it('should return new date from json date', () => {
    const date = `${year}-${month}-${day}T00:00:00.000Z`;
    const res = dateFromJson(date);
    expect(res.getDate()).toBe(day);
    expect(res.getMonth() + 1).toBe(month);
    expect(res.getFullYear()).toBe(year);
  });
});
