describe('datetimeToInput', () => {
  const datetimeToInput = require('../../../lib/helpers/datetime-to-input').DatetimeToInput;
  const year = 2016;
  const month = 10; // October
  const day = 12;
  const hours = 10;
  const minutes = 30;
  const date = `${year}-${month}-${day}T${hours}:${minutes}:00.000Z`;

  it('should return the correct format', () => {
    expect(datetimeToInput(date)).toBe(`${year}-${month}-${day}T${hours}:${minutes}`);
  });
});
