describe('formatPrice', () => {
  const formatPrice = require('../../../lib/helpers/format-price').FormatPrice;
  const price1 = 1000; // 10 eur
  const price2 = 1025; // 10.25 eur
  const price3 = 1055.5; // 10.555 eur
  const currency = "GBP";

  it('should return the correct format', () => {
    expect(formatPrice(price1)).toBe("10.00€");
    expect(formatPrice(price2, "GBP")).toBe("10.25£");
    expect(formatPrice(price3, "USD")).toBe("10.55$");
  });
});
