describe('cardValidator', () => {
  const cardValidator = require('../../../lib/helpers/card-validator').CardValidator;
  const valid1 = "4706750000000009";
  const valid2 = "3569990000000132";
  const invalid1 = "1234567890123456";
  const invalid2 = "9876";

  it('should be valid cards', () => {
    expect(cardValidator(valid1)).toBe(true);
    expect(cardValidator(valid2)).toBe(true);
  });

  it('should be invalid cards', () => {
    expect(cardValidator(invalid1)).toBe(false);
    expect(cardValidator(invalid2)).toBe(false);
  });
});
