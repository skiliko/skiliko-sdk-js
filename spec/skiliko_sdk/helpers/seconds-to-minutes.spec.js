describe('secondsToMinutes', () => {
  const secondsToMinutes = require('../../../lib/helpers/seconds-to-minutes').SecondsToMinutes;
  const seconds1 = 0 * 60 + 10;
  const seconds2 = 1 * 60;
  const seconds3 = 1 * 60 + 30;
  const seconds4 = 5 * 60;
  const seconds5 = 6 * 60 + 5;

  it('should display minutes and seconds', () => {
    expect(secondsToMinutes(seconds1)).toBe("0m10s");
    expect(secondsToMinutes(seconds2)).toBe("1m00s");
    expect(secondsToMinutes(seconds3)).toBe("1m30s");
    expect(secondsToMinutes(seconds4)).toBe("5m00s");
    expect(secondsToMinutes(seconds5)).toBe("6m05s");
  });
});
