describe('countries', () => {
  const countries = require('../../lib/modules/country');

  it('should create fetch all countries', (done) => {
    countries.all()
    .then(res => {
      expect(Object.keys(res).length).toBe(260);
      done();
    });
  });
  
  it('should contains codes', (done) => {
    countries.all()
    .then(res => {
      expect(res.filter(country => country.code === 'TH').length).toBe(1);
      done();
    });
  });
});
