describe('settings', () => {
  const settings = require('../../lib/modules/settings');
  const key = 'key';
  const value = 'value';

  it('should write settings', () => {
    settings.set(key, value);
    expect(settings.get(key)).toBe(value);
  });

  it('should delete setting', () => {
    settings.set(key, value);
    settings.remove(key);
    expect(settings.get(key)).toBe(null);
  });

  it('should have default value', () => {
    expect(settings.get('test', 'ok')).toBe('ok');
  });
});
