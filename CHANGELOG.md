## 3.3.3 (July 13, 2020)

* Improve user birthday detection

## 3.3.2 (July 13, 2020)

* Improve user birthday detection

## 3.3.1 (January 5, 2020)

* Add missing online information on the expert

## 3.3.0 (December 15, 2019)

* Add `onPurchaseUpdate` on chat

## v3.2.1 (June 9, 2019)

* Fix issue with passing reason as argument

## v3.2.0 (June 9, 2019)

* Possiblity to add a reason when calling the refund method

## v3.1.0 (March 11, 2018)

* Add `Offer.publicOffers` function to get all public offers

## v3.0.0 (August 17, 2017)

* Fix some compilation issues that can lead to issues using the lib on the server side with `Settings`
* `Purchase.all` now use a new API with more way to query your purchases. You can now give a payload to `Purchase.all` with the following params:

| parameter | type | default | comment |
| --- | --- | --- | --- |
| scope | `"all"`, `"payed"`, `"refunded"`, `"pending"`, `"current"`, `"finished"`, `"with_cash_received"`, `"finished_with_cash_received_or_failed"` | `"all"` | the type of purchase you want to fetch |
| page | number | `1` | pagination of 15 items per page |
| user_id | number | `null` | will be ignored if you fetch as an user |
| admin_id | number | `null` | will be ignored if you fetch as an admin |
| offer_id | number | `null` | purchase from a specific offer |
| service_type | `"phone"`, `"service"`, `"prepaid_minute"`, `"service"` | `null` | filter the purchase for a specific service |

## v2.15.0 (Jul 04, 2017)

* `Expert.available` method now return false if the expert is doing a break even during the time (s)he should be available
* fix warning for deprecated methods in `Expert` module

## v2.14.1 (May 15, 2017)

* Do not make a request for user not connected (not token to revoke so request is useless)

## v2.14.0 (May 12, 2017)

* Expert module expose now the following functions:
  - `todaySchedule(expert)`: to know the schedule of today in the local timezone
  - `nextSchedule(expert)`: to know the next schedule in the local timezone
  - `available(expert)`: to know if the expert is available
  - `isRestricted(expert)`: to know if the expert is restricted

## v2.13.0 (May 9, 2017)

* Partial support for Server Side rendering. Now you can use the SDK with requests, the platform and the settings will be persistant for every request and all the api calls will have the righe signature

## v2.12.0 (May 8, 2017)

* Add support for restricted experts and function `expert.isRestricted()` to check if an expert is restricted

## v2.11.0 (April 17, 2017)

* Possibility to add data when send message on chat `send(message: string, to: string, data: Object = null): Promise<any>`

## v2.10.5 (April 11, 2017)

* fix issue when bulding in typescript due to one missing dependency

## v2.10.4 (April 11, 2017)

* better management to display hours until 24h and not 23:59

## v2.10.3 (April 04, 2017)

* manage wrong query when session is not set yet even the platform has been fetched

## v2.10.2 (April 01, 2017)

* do not allow phone not completed to be rated

## v2.10.1 (March 30, 2017)

* Fix issue with timezone containing `&` character
* Fix expert availability with correct timezone offset

## v2.10.0 (March 26, 2017)

* Check expert availability according to the schedule and the last active call (before only the schedule was checked)

## v2.9.3 (March 14, 2017)

* issue with cached promise for current user. The promise in cache was not transforming the data

## v2.9.2 (March 13, 2017)

* fix issue with user birthday convertion

## v2.9.1 (March 11, 2017)

* fix issue when convert user birthday date

## v2.9.0 (March 02, 2017)

* add method `customOffers` to fetch all custom offers of a platform is this platform has the custom offers enable

## v2.8.0 (February 23, 2017)

* Possibility to buy an offer for a specific expert when calling `Purchase.create(offer, expert)`

## v2.7.0 (February 16, 2017)

* Possibility to pass the expert id when fetch ratings `Rating.all(page = 1, expertId = null)`. If the `expertId` is given only the ratings received from this expert will be returned

## v2.6.0 (February 13, 2017)

* process expert to add :
  - `localSchedule`: [Array] the expert's schedule based on the time on the local computer 
  - `todaySchedule`: [function] the expert's schedule for the current day
  - `nextSchedule`: [function] the next availability for the expert
  - `available`: [function] the availability of the expert
* [bug]: fix issue when pass a null conversationId on `chat.messages` function
* [bug]: when a conversation is received from the socket, update it with the with property

## v2.5.1 (February 10, 2017)

* deprecate `conversation` method and fix issues with `conversationWith` method

## v2.5.0 (February 10, 2017)

* Add `conversationWith` method in chat with to get the conversation with a specific user

## v2.4.1 (February 08, 2017)

* Fix issue when passe an `null` value to `DateToInput` (bug when update user birthday and no birthday is set)

## v2.4.0 (February 02, 2017)

* add `Expert` service to fetch experts of the platform (usefull for multi expert platform)
* return status of a call one the `chat.onCallUpdate` callback

## v2.3.4 (January 31, 2017)

* [BUG] fix bug when try to update user information when it already has a valid value for birthday

## v2.3.3 (January 30, 2017)

* manage acceptance of purchase according to the new multi expert api
  

## v2.3.2 (January 21, 2017)

* [BUG]: fix typo and wrong status for purchase

## v2.3.1 (January 21, 2017)

* [BUG]: error when accept a purchase

## v2.3.0 (January 20, 2017)

* Add extra data to purchase: 
  * `need_rating`: a `boolean` to know if the purchase needs to be rated
  * `status`: To know the status of the purchase
    * `pending`: wait for the expert to accept the purchase
    * `accepted`: the purchase has been accepted, waiting for the expert to finish it
    * `finished`: the purchase is finished and the expert fot his money
    * `refunded`: the purchase has been refunded

## v2.2.0 (January 19, 2017)

  * Add new function `ratingNeeded(purchase: Object)` in purchase module to know if the the purchase needs to be rated

## v2.1.0 (January 18, 2017)

* [BUG]: error when try to save a user without giving birthday date fixed
* [BUG]: wrong endpoint to fetch pending messages
* `Chat.getChat` reject the promise when the socket cannot connect
* `chat.conversations` returns now a `with` data to know with who the discussion is with without testing the from/to
* `chat.send` will reject the promise if the `message` or the `to` param is empty

## v2.0.2 (January 12, 2017)

* [BUG]: fix issue from 3rd tier for card tokenization when sending parameter in the wrong format

## v2.0.0 (January 11, 2017)

* `User.save` will automatically parse the birthday into the right format. You can now:
  * Give like before the date in ms
  * Give the date in `YYYY-MM-DD` format

### Breaking changes

* `User.current()` return a promise of an user with the birthday formatted as follow: `YYYY-MM-DD` like that this data can directly fit into a Date object or an input date
