import axios from "axios";
import * as Request from "./_request-api";
import { endpoints } from "../data/constants";

const preregister = (): Promise<any> => {
  return Request.sendPost(`${endpoints.api}/preregistered_cards`);
};

const tokenize = (cardNumber, cardMonth, cardYear, cardCvx, data): Promise<any> => {
  const headers = {
    "Content-Type": "text/plain",
  };
  const params = {
    data: data.preregistration_data,
    accessKeyRef: data.access_key,
    cardNumber,
    cardCvx,
    cardExpirationDate: [
      cardMonth,
      cardYear,
    ].join(""),
  };
  return <Promise<any>>axios({
    url: data.card_registration_url,
    method: "POST",
    headers,
    data: Object.keys(params).map(k => [k, params[k]].join("=")).join("&"),
  }).then(response => response.data);
};

const finalize = (preregisteredCard, token): Promise<any> => {
  return Request.sendPut(`${endpoints.api}/cards/${preregisteredCard.id}`, {
    registration_data: token,
  });
};

const create = (cardNumber, month, year, cvx): Promise<any> => {
  return preregister()
  .then(preregisteredCard => {
    return tokenize(cardNumber, month, year, cvx, preregisteredCard)
    .then(token => finalize(preregisteredCard, token));
  });
};

export {
  create,
};
