import * as Request from "./_request-api";
import { endpoints } from "../data/constants";
import * as Schedule from "./timezone";

const localSchedule = expert => Schedule.scheduleToCurrentDates(expert.schedule, expert.timezone, expert.timezone_offset);

const todaySchedule = expert => localSchedule(expert)[(new Date()).getDay()]
  .find(d => d["from"] < new Date() && d["to"] > new Date());

const nextSchedule = expert => localSchedule(expert)
  .reduce((a, b) => a.concat(b))
  .sort((a, b) => a["from"] - b["from"])
  .find(e => e["from"] > new Date());

const available = expert => {
  if (expert.current_pause) { return false; }
  if (expert.last_active_call) { return false; }
  const today = localSchedule(expert)[(new Date()).getDay()];
  if (!today) { return false; }
  return !!today.find(d => d["from"] < new Date() && d["to"] > new Date());
};

const isRestricted = expert => expert.role === "restricted";

const deprecate = (method, result) => {
  console.warn(`[DEPRECATED] expert.${method}() is now deprecated, use Expert.${method}(expert) instead`);
  return result;
};

const processExpert = expert => {
  return Request.sendGet(`${endpoints.chat}/online?userId=${expert.chatable_id}`)
  .then(online => {
    return Object.assign({}, expert, {
      online: online,
      localSchedule: localSchedule(expert),
      todaySchedule: () => deprecate("todaySchedule", todaySchedule(expert)),
      nextSchedule: () => deprecate("nextSchedule", nextSchedule(expert)),
      available: () => deprecate("available", available(expert)),
      isRestricted: () => deprecate("isRestricted", isRestricted(expert))
    });
  });
};

const all = (page: number = 1): Promise<any> => {
  return Request.sendGet(`${endpoints.api}/experts`)
  .then(experts => Promise.all(experts.map(processExpert)));
};

const get = (id): Promise<any> => {
  return Request.sendGet(`${endpoints.api}/experts/${id}`)
  .then(processExpert);
};

export {
  all,
  get,
  todaySchedule,
  nextSchedule,
  available,
  isRestricted
};
