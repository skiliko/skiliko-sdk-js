import * as Request from "./_request-api";
import * as Platform from "./platform";
import { endpoints } from "../data/constants";

const filter = (condition: Function): Promise<any> => Platform.current()
  .then(platform => platform
    ? platform["offers"].filter(condition)
    : []
  );

const all = (type: string): Promise<any> => filter(e => e["service_type"] === type);

const prepaidMinutes = (): Promise<any> => all("prepaid_minute");

const phone = (): Promise<any> => all("phone")
  .then(offers => offers[0]);

const promotions = (): Promise<any> => filter(e => e["promotion"]);

const customOffers = (): Promise<any> => Platform.current()
  .then(platform => platform.settings.custom_offers
    ? Request.sendGet(`${endpoints.api}/custom_offers`)
    : []
  );

const publicOffers = (): Promise<any> => Request
  .sendGet(`${endpoints.api}/public_offers`);

const get = (id): Promise<any> => Request
  .sendGet(`${endpoints.api}/offers/${id}`);

export {
  all,
  get,
  promotions,
  publicOffers,
  prepaidMinutes,
  phone,
  customOffers,
};
