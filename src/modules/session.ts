import * as Request from "./_request-api";
import * as Settings from "./settings";
import { endpoints } from "../data/constants";

const signedInCallbacks: Function[] = [];
const signedOutCallbacks: Function[] = [];
const pingInterval: number = 5 * 60 * 1000;
var pingId: number;
var sessionStarted: boolean = false;

const ping = (): Promise<any> => {
  return Request
  .sendPost(`${endpoints.api}/user/ping`);
};

const updateCallback = (list: Function[], callback: Function): Function => {
  const index = list
  .map(f => f.toString())
  .indexOf(callback.toString());
  if (index < 0) {
    list.push(callback);
  } else {
    list[index] = callback;
  }
  return callback;
};

const whenLogin = (token) => {
  Settings.set("token", token);
  signedInCallbacks.forEach(callback => {
    if (callback && sessionStarted) { callback(token); }
  });
  return token;
};

const whenLogout = (platformToken = null) => {
  Settings.set("token", {
    platform_token: (Settings.get("token") || {}).platform_token || platformToken,
  });
  signedOutCallbacks.forEach(callback => {
    if (callback && sessionStarted) { callback(); }
  });
  return;
};

const signedIn = () => {
  const token = Settings.get("token");
  if (!token) { return false; }
  if (!token.access_token) { return false; }
  if (!token.expires_in) { return true; }
  const expire = token.created_at + token.expires_in;
  const now = (new Date()).getTime() / 1000;
  return expire > now;
};

const login = (email, password, scope = "user", platformName = null): Promise<any> => {
  const headers = scope === "admin" ? { PlatformTokenName: platformName } : {};
  return Request
  .sendPost(`${endpoints.api}/oauth/token`, {
    grant_type: "password",
    username: email,
    password,
    scope,
  }, headers)
  .then(whenLogin);
};

const logout = (): Promise<any> => {
  const token = Settings.get("token") || {};
  const accessToken = (Settings.get("token") || {}).access_token;
  return (token.access_token
    ? Request.sendPost(`${endpoints.api}/oauth/revoke`, { token: accessToken })
    : Promise.resolve())
  .then(whenLogout);
};

const register = (params): Promise<any> => {
  return Request
  .sendPost(`${endpoints.api}/users`, params)
  .then(whenLogin);
};

const unregister = (): Promise<any> => {
  return Request
  .sendDelete(`${endpoints.api}/user`)
  .then(whenLogout);
};

const onSignedIn = (callback) => {
  updateCallback(signedInCallbacks, callback);
  if (signedIn() && callback && sessionStarted) { setTimeout(callback); }
};

const onSignedOut = (callback) => {
  updateCallback(signedOutCallbacks, callback);
  if (!signedIn() && callback && sessionStarted) { setTimeout(callback); }
};

const init = (platformToken) => {
  sessionStarted = true;
  if (signedIn()) {
    whenLogin(Settings.get("token", {
      platform_token: platformToken,
    }));
  } else {
    whenLogout(platformToken);
  }
};

onSignedIn(() => {
  ping();
  pingId = pingId || setInterval(() => {
    if (!pingId) { return; }
    ping();
  }, pingInterval);
});

onSignedOut(() => {
  if (!pingId) { return; }
  clearInterval(pingId);
  pingId = null;
});

export {
  init,
  signedIn,
  login,
  logout,
  register,
  unregister,
  onSignedIn,
  onSignedOut,
};
