import * as Session from './session';
import * as Request from './_request-api';
import { endpoints } from "../data/constants";

const sendInstructions = (email: string, resource: string = 'User'): Promise<any> => {
  return Request.sendPost(`${endpoints.api}/passwords`, {
    email,
    resource_type: resource,
  });
};

const reset = (token: string, password: string, confirmation: string, resource: string = 'User'): Promise<any> => {
  return Request.sendPut(`${endpoints.api}/passwords`, {
    reset_password_token: token,
    password,
    password_confirmation: confirmation,
    resource_type: resource,
  });
};

export {
  sendInstructions,
  reset,
};
