import * as Request from './_request-api';
import * as User from "./user";
import { endpoints } from "../data/constants";

const all = (page: number = 1, expertId = null) => {
  const endpoint = expertId ? `${endpoints.api}/experts/${expertId}/ratings` : `${endpoints.api}/ratings`;
  return Request.sendGet(endpoint, {
    page,
  });
};

const create = (purchase: number, note: number, content: string = null) => {
  return Request.sendPost(`${endpoints.api}/ratings`, {
    note,
    purchase_id: purchase,
    content,
  })
  .then(rating => {
    User.current({ forceFetch: true });
    return rating;
  });
};

export {
  all,
  create,
};
