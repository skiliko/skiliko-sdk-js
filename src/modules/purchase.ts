import * as Request from "./_request-api";
import * as User from "./user";
import { endpoints } from "../data/constants";

export interface IPurchasesQuery {
    scope?:
    "all" |
    "payed" |
    "refunded" |
    "pending" |
    "current" |
    "finished" |
    "with_cash_received" |
    "finished_with_cash_received_or_failed";
    page?: number;
    user_id?: number;
    admin_id?: number;
    offer_id?: number;
    service_type?:
    "phone" |
    "service" |
    "prepaid_minute" |
    "mail";
}

const processPurchase = (purchase: Object | Object[]) => {
    if (!purchase) { return purchase; }
    if (Array.isArray(purchase)) {
        // Do not return a map to avoid duplicating the purchases.pagination
        purchase.forEach(processPurchase);
        return purchase;
    }
    return Object.assign(purchase, {
        need_rating: ratingNeeded(purchase),
        status: status(purchase),
    });
};

const status = (purchase: Object): String => {
    if (purchase["refunded_at"]) { return "refunded"; }
    if (purchase["finished_at"]) { return "finished"; }
    if (purchase["accepted_at"]) { return "accepted"; }
    return "pending";
};

const all = (query: IPurchasesQuery): Promise<any> => {
    return Request.sendGet(`${endpoints.api}/purchases_v2`, Object.assign({}, {
        page: 1,
        scope: "all"
    }, query))
        .then(processPurchase);
};

const get = (id: string): Promise<any> => {
    return Request.sendGet(`${endpoints.api}/purchases/${id}`)
        .then(processPurchase);
};

const create = (offer: Object, expert: Object = null): Promise<any> => {
    const data = {
        offer_id: offer["id"],
    };
    if (expert) {
        data["admin_id"] = expert["id"];
    }
    return Request.sendPost(`${endpoints.api}/purchases`, data)
        .then(processPurchase);
};

const save = (purchase: Object, data: Object): Promise<any> => {
    const serviceType = purchase["offer"]["service_type"];
    return Request.sendPut(`${endpoints.api}/${serviceType}_services/${purchase["id"]}`, data)
        .then(processPurchase);
};

const pay = (purchase: Object): Promise<any> => {
    return Request.sendPost(`${endpoints.api}/purchase_payments`, {
        purchase_id: purchase["id"],
        card_id: purchase["card_id"],
    })
        .then(processPurchase);
};

const canAccept = (purchase: Object): boolean => {
    if (purchase["accepted_at"]) { return false; }
    if (purchase["refunded_at"]) { return false; }
    return purchase["service"]["creator_type"] === "Admin";
};

const accept = (purchase: Object): Promise<any> => {
    if (!canAccept(purchase)) {
        throw `Cannot accept purchase ${purchase["id"]}`;
    }

    return Request.sendPost(`${endpoints.api}/accept_purchases`, {
        purchase_id: purchase["id"],
    })
        .then(processPurchase);
};

const canRefund = (purchase: Object): boolean => {
    if (purchase["service_type"] === "call") { return false; }
    if (!!purchase["refunded_at"]) { return false; }
    if (!purchase["payed_at"]) { return false; }
    if (!!purchase["finished_at"]) { return false; }
    return true;
};

const refund = (purchase: Object): Promise<any> => {
    if (!canRefund(purchase)) {
        throw `Cannot refund purchase ${purchase["id"]}`;
    }
    return Request.sendPost(`${endpoints.api}/refunds`, {
        purchase_id: purchase["id"],
        reason: purchase["reason"],
    })
        .then(processPurchase);
};

const toReview = (): Promise<any> => {
    return Request.sendGet(`${endpoints.api}/waiting_for_review_purchases`)
        .then(processPurchase);
};

const ratingNeeded = (purchase: Object): boolean => {
    if (!purchase["finished_at"]) { return false; }
    if (purchase["rating"]) { return false; }
    if (purchase["refunded_at"]) { return false; }
    if (purchase["offer"]["service_type"] === "prepaid_minute") { return false; }
    if (purchase["offer"]["service_type"] === "phone") { return purchase["service"]["call"]["status"] === "completed"; }
    return true;
};

export {
    all,
    get,
    create,
    save,
    pay,
    canAccept,
    accept,
    canRefund,
    refund,
    toReview,
    ratingNeeded,
};
