import * as Request from "./_request-api";
import * as Settings from "./settings";
import { endpoints, environment } from "../data/constants";

const platformToken = () => {
  return Settings.get("token", {}).platform_token;
};

const current = (): Promise<any> => {
  if (environment.cache["platformPromise"]) { return environment.cache["platformPromise"]; }
  if (environment.cache["platform"]) {
    environment.cache["platformPromise"] = Promise.resolve(environment.cache["platform"]);
  } else {
    environment.cache["platformPromise"] = Request
    .sendGet(`${endpoints.api}/platform`, {}, (environment["request"] || {})["headers"]);
  }
  return environment.cache["platformPromise"]
  .then(p => {
    if (p) {
      environment.cache["platform"] = p;
      Settings.set("locale", p["locale"]);
      Settings.set("token", Object.assign({}, Settings.get("token"), {
        platform_token: p["token"]
      }));
    }
    return p;
  });
};

const update = (data): Promise<any> => {
  return Request
  .sendPut(`${endpoints.api}/platform/${platformToken()}`, data)
  .then(p => {
    environment.cache["platform"] = p;
    return p;
  });
};

const updateSettings = (data): Promise<any> => {
  return update({ settings: JSON.stringify(data) });
};

// TODO: make it async
const currency = () => {
  return (environment.cache["platform"] || {}).currency;
};

export {
  current,
  update,
  updateSettings,
  currency,
};
