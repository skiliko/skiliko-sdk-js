import * as Request from "./_request-api";
import * as Platform from "./platform";
import { endpoints } from "../data/constants";

const getUploadUrl = (platform, file, path): Promise<any> => {
  const fullPath = [
    "platforms",
    platform.id,
    path,
  ].filter(e => e).join("/");
  return Request.sendPost(endpoints.upload, {
    path: fullPath,
    file_name: file ? parameterize(file.name) : null,
    file_type: file ? encodeURIComponent(file.type) : null,
  });
};

const uploadToUrl = (data, file): Promise<any> => {
  // TODO find equivalent with fetch API
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', data['signed_request']);
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== 4) { return; }
      if (xhr.status !== 200) { reject(); }
      resolve({
        url: data['url'],
        name: file.name,
        type: file.type,
      });
    };
    xhr.send(file);
  });
};

const resize = (path): Promise<any> => {
  return Request.sendPost(`${endpoints.upload}/resize`, {
    key: path,
  });
};

const parameterize = (value) => {
  const transformation = value.toLowerCase()
  .replace(/[^a-z0-9]+/g, "-")
  .replace(/(^-|-$)/g, "");
  return encodeURIComponent(transformation);
};

const upload = (file, path): Promise<any> => {
  return Platform.current()
  .then(platform => {
    return getUploadUrl(platform, file, path)
    .then(data => uploadToUrl(data, file))
    .then(data => resize(data.url));
  });
};

export {
  upload,
};
