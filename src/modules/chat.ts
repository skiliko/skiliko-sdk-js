import * as socketIO from "socket.io-client";
import * as Request from "./_request-api";
import * as Session from "./session";
import { endpoints } from "../data/constants";
import { CallWithStatus } from "../helpers/call-status";

const chatServers: ChatServer[] = [];
const signInRequiredError: Error = new Error("Signin required");

class ChatServer {
  public from: string;
  private socket: SocketIOClient.Socket;

  constructor(socket: SocketIOClient.Socket, from: string) {
    this.socket = socket;
    this.from = from;
  }

  onNewMessage(callback: Function): ChatServer {
    return this.connect("messages:created", callback);
  }

  onMessageRead(callback: Function): ChatServer {
    return this.connect("messages:read", callback);
  }

  onMessageReceived(callback: Function): ChatServer {
    return this.connect("messages:received", callback);
  }

  onMessageUpdate(callback: Function): ChatServer {
    return this.connect("messages:updated", callback);
  }

  onNewConversation(callback: Function): ChatServer {
    return this.connect("conversations:update", conversation => callback(Object.assign({}, conversation, {
      with: conversation.from._id === this.from ? conversation.to : conversation.from
    })));
  }

  onCallUpdate(callback: Function): ChatServer {
    return this.connect("call:updated", call => callback(CallWithStatus(call)));
  }

  onPurchaseUpdate(callback: Function): ChatServer {
    return this
      .connect("purchase:create", callback)
      .connect("purchase:update", callback)
  }

  markAsReceived(id: string): Promise<any> {
    return Request.sendPut(`${endpoints.chat}/messages/${id}`, {
      user: this.from,
      receivedAt: new Date(),
    });
  }

  markAsRead(ids: string[]): Promise<any> {
    return Request.sendPost(`${endpoints.chat}/read`, {
      ids,
      user: this.from,
      readAt: new Date(),
    });
  }

  send(message: string, to: string, data: Object = null): Promise<any> {
    if (!message || message.trim() === "") { return Promise.reject({ message: "empty" }); }
    if (!to) { return Promise.reject({ to: "empty" }); }
    return Request.sendPost(`${endpoints.chat}/messages`, {
      from: this.from,
      to,
      content: message,
      sentAt: new Date(),
      data: JSON.stringify(data)
    });
  }

  conversations(page: number = 1): Promise<any> {
    return Request.sendGet(`${endpoints.chat}/users/${this.from}/conversations`, {
      page,
    })
    .then(conversations => conversations.map(conversation => Object.assign({}, conversation, {
      with: conversation.from._id === this.from ? conversation.to : conversation.from
    })));
  }

  messages(conversationId, page = 1): Promise<any> {
    if (!conversationId) { return Promise.resolve([]); }
    return Request.sendGet(`${endpoints.chat}/conversations/${conversationId}/messages`, {
      page,
    });
  };

  conversation(): Promise<any> {
    console.warn("this method is deprecated, please use conversationWith instead");
    return Request.sendPost(`${endpoints.chat}/conversations/search`, {
      userId: this.from,
    });
  };

  conversationWith(userId): Promise<any> {
    return Request.sendPost(`${endpoints.chat}/conversations/search`, {
      from: userId,
      to: this.from
    })
    .then(conversation => Object.assign({}, conversation, {
      with: conversation.from._id === this.from ? conversation.to : conversation.from
    }))
    .catch(() => ({
      from: { _id: this.from },
      to: { _id: userId },
      with: { _id: userId }
    }));
  };

  private connect(eventName: string, callback: Function): ChatServer {
    this.socket.on(eventName, callback);
    return this;
  }
}

const getChat = (from: string): Promise<any> => {
  if (!Session.signedIn()) { throw signInRequiredError; }
  return new Promise((resolve, reject) => {
    const chatServer = chatServers.find(e => e.from === from);
    if (chatServer) { return resolve(chatServer); }

    const socket: SocketIOClient.Socket = socketIO(endpoints.chat, {
      transports: ["websocket"],
      query: [
        "user",
        from,
      ].join("="),
    });

    socket.on("disconnect", () => {
      const index = chatServers.findIndex(e => e.from === from);
      if (index >= 0) {
        delete chatServers[index];
        chatServers.splice(index, 1);
      }
    });
    socket.on("connect", () => {
      const chatServer = new ChatServer(socket, from);
      chatServers.push(chatServer);
      resolve(chatServer);
    });
    socket.on("error", reject);
    return socket;
  });
};

const pendingMessages = (user): Promise<any> => {
  return Request.sendGet(`${endpoints.chat}/pendings`, {
    user,
  });
};

export {
  ChatServer,
  getChat,
  pendingMessages,
};
