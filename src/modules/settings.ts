import { environment } from "../data/constants";

const STORATE_KEY = "settings";

class LocalStorageStore {
  getItem(key) { return localStorage.getItem(key); }
  setItem(key, value) { localStorage.setItem(key, value); }
}

class CacheStorage {
  getItem(key) { return environment.cache[key]; }
  setItem(key, value) { environment.cache[key] = value; }
}

const createStore = () => {
  if (typeof localStorage === "undefined") {
    return new CacheStorage();
  } else {
    return new LocalStorageStore();
  }
};

const store = createStore();

const storage = () => JSON.parse(store.getItem(STORATE_KEY) || "{}") || {};

const save = (data) => store.setItem(STORATE_KEY, JSON.stringify(data));

const get = (key, defaultValue = null) => storage()[key] || defaultValue || null;

const set = (key, value) => {
  const newStorage = storage();
  newStorage[key] = value;
  save(newStorage);
  return value;
};

const remove = (key) => {
  const newStorage = storage();
  delete newStorage[key];
  save(newStorage);
};

export {
  get,
  set,
  remove,
};
