import axios from "axios";
import * as Settings from "./settings";
import { endpoints } from "../data/constants";

const defaultHeader = (): Object => {
  const token = Settings.get("token", {});
  const headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
  };
  if (token.platform_token) {
    headers["PlatformToken"] = token.platform_token;
  }
  if (token.access_token && token.token_type) {
    headers["Authorization"] = [
      token.token_type,
      token.access_token,
    ].join(" ");
  }
  return headers;
};

const extractPagination = (result) => {
  const headerValue = (key) => {
    const value = result.headers[key] || result.headers[key.toLowerCase()];
    return value && parseInt(value, 10);
  };
  if (!headerValue('X-Page')) { return null; }
  return {
    total:      headerValue('X-Total'),
    totalPages: headerValue('X-Total-Pages'),
    page:       headerValue('X-Page'),
    perPage:    headerValue('X-Per-Page'),
    nextPage:   headerValue('X-Next-Page'),
    prevPage:   headerValue('X-Prev-Page'),
    offset:     headerValue('X-Offset'),
  };
};

const request = (url: string, method: string, data: Object = {}, headers: Object = {}): Promise<any> => {
  if (!endpoints.api) { throw new Error("Skiliko SKD not initialized"); }

  const config = {
    url,
    method,
    headers: Object.assign({}, defaultHeader(), headers),
  };

  if (Object.keys(data).length > 0) {
    config[method === "get" ? "params" : "data"] = data;
  }

  return <Promise<any>>axios(config)
  .then(response => {
    const data = response.data;
    if (!data || !Array.isArray(data)) { return data; }
    data["pagination"] = extractPagination(response);
    return data;
  });
};

const sendGet = (url: string, data: Object = {}, headers: Object = {}): Promise<any> => {
  const dataToParams = Object.keys(data)
  .map(key => [key, data[key]].join("="))
  .join("&");
  if (url.indexOf("?") <= 0) { url += "?"; }
  url += dataToParams;
  return request(url, "get", {}, headers);
};

const sendPost = (url: string, data: Object = {}, headers: Object = {}): Promise<any> => {
  return request(url, "post", data, headers);
};

const sendPut = (url: string, data: Object = {}, headers: Object = {}): Promise<any> => {
  return request(url, "put", data, headers);
};

const sendDelete = (url: string, data: Object = {}, headers: Object = {}): Promise<any> => {
  return request(url, "delete", data, headers);
};

export {
  sendPost,
  sendPut,
  sendGet,
  sendDelete,
};
