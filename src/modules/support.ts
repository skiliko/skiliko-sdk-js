import * as Request from "./_request-api";
import { endpoints } from "../data/constants";

const message = (data): Promise<any> => {
  return Request.sendPost(`${endpoints.api}/supports`, data);
};

export {
  message,
};
