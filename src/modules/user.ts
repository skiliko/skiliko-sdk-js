import { DateToInput } from "../helpers/date-to-input";
import * as Session from "./session";
import * as Request from "./_request-api";
import { endpoints } from "../data/constants";

let user: Object = null;
let promise: Promise<Object> = null;

const updateUser = (userData) => {
  if (!userData["birthday"]) { return userData; }
  const birthday = typeof userData["birthday"] === "string"
      ? new Date(userData["birthday"]).getTime()
      : userData["birthday"] * 1000
  return Object.assign({}, userData, {
    birthday: DateToInput(birthday),
  });
};

const current = (options: Object = {}): Promise<any> => {
  if (options["forceFetch"]) {
    user = promise = null;
  }
  if (user) { return Promise.resolve(user); }
  if (promise) { return promise; }
  promise = Request.sendGet(`${endpoints.api}/user`)
  .then(updateUser);
  return promise;
};

const save = (data: Object): Promise<any> => {
  if (data["birthday"] && parseInt(data["birthday"], 10) !== data["birthday"]) {
    data["birthday"] = +new Date(data["birthday"]) / 1000;
  }
  return Request.sendPut(`${endpoints.api}/user`, data)
  .then(updateUser);
};

const updateWith = (data: Object) => {
  if (!user) { return; }
  updateUser(Object.assign({}, user, data));
};

Session.onSignedOut(() => {
  user = promise = null;
});

export {
  current,
  save,
  updateWith,
};
