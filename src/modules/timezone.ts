import { TimezoneData } from "../data/timezones";

const getTimeZone = (name) => {
  return TimezoneData.find(timezone => timezone.name === name);
};

const timeInCurrentZone = (day, hour, minute, timezone, timezoneOffset) => {
  const offset = timezoneOffset || getTimeZone(timezone).offset;
  const curr = new Date();
  const date = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate());
  const dayToAdd = ((day - date.getDay()) + 7) % 7;
  const minutesToAdd = 0
    + dayToAdd * 24 * 60
    + hour * 60
    + minute
    - (date.getTimezoneOffset() + offset); // shift the zone from the current to the new
  date.setTime(date.getTime() + minutesToAdd * 60 * 1000);
  return date;
};

const hourString = (hours, minutes) => {
  return [
    `0${hours.toString()}`.slice(-2),
    `0${minutes.toString()}`.slice(-2)
  ].join(":");
};

const offsetToHourString = (offset) => {
  const hoursWithSign = Math.trunc(offset / 60);
  const hours = Math.abs(hoursWithSign);
  const negative = hoursWithSign < 0;
  const minutes = offset % 60;
  return [
    negative ? "-" : "+",
    hourString(hours, minutes),
  ].join("");
};

const detect = () => {
  const offset = -(new Date()).getTimezoneOffset();
  return TimezoneData.find(timezone => timezone.offset === offset);
};

const getSelectOptions = () => {
  TimezoneData.map(timezone => ({
    label: `(GMT${offsetToHourString(timezone.offset)}) ${timezone.name}`,
    value: timezone.name,
  }));
};

const scheduleToCurrentDates = (schedule, timezone, timezoneOffset) => {
  const days = [[], [], [], [], [], [], []];
  schedule
  .filter(s => s.from || s.to)
  .forEach(s => {
    if (s.from || s.to) {
      const [fHour, fMin] = (s.from || "00:00").split(":").map(e => parseInt(e, 10));
      const [tHour, tMin] = (s.to || "24:00").split(":").map(e => parseInt(e, 10));
      const from = timeInCurrentZone(s.day, fHour, fMin, timezone, timezoneOffset);
      const to = timeInCurrentZone(s.day, tHour, tMin, timezone, timezoneOffset);
      if (from.getDay() === to.getDay()) {
        days[from.getDay()].push({
          from,
          to,
        });
      } else {
        const newTo = new Date(from.getFullYear(), from.getMonth(), from.getDate(), 24, 0);
        const newFrom = new Date(to.getFullYear(), to.getMonth(), to.getDate(), 0, 0);
        if (from.getTime() !== newTo.getTime()) {
          days[from.getDay()].push({
            from,
            to: newTo,
          });
        }
        if (newFrom.getTime() !== to.getTime()) {
          days[to.getDay()].push({
            from: newFrom,
            to,
          });
        }
      }
    }
  });
  return days.map(day => day.sort((a, b) => a.from - b.from));
};

export {
  detect,
  getSelectOptions,
  scheduleToCurrentDates,
};
