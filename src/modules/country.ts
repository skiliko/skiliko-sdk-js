import axios from "axios";
import * as Settings from "./settings";
const base = "https://skiliko.firebaseio.com/translations";
const path = "collection/countries.json";

const sort = (countries) => {
  const transform = (code) => {
    return { code, name: countries[code] };
  };
  return Object.keys(countries)
  .map(transform)
  .sort((c1, c2) => c1.name.localeCompare(c2.name));
};

const all = (locale): Promise<any> => {
  const url = [
    base,
    locale || Settings.get("locale") || "en",
    path,
  ].join("/");
  return <Promise<any>>axios(url)
  .then(response => response.data)
  .then(countries => sort(countries));
};

const codes = (locale): Promise<any> => {
  return all(locale)
  .then(countries => countries.map(e => e.code));
};

export {
  codes,
  all,
};
