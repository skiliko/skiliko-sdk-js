export interface Endpoints {
  api: string;
  upload: string;
  chat: string;
}

const endpoints: Endpoints = {
  api: null,
  upload: null,
  chat: null,
};

const environment = {
  request: null,
  isNode: false,
  cache: {},
};

const init = (domain: string | Endpoints = "skiliko.com") => {
  if (typeof domain === "string") {
    endpoints.api = `https://api.${domain}`;
    endpoints.upload = `https://upload.api.${domain}`;
    endpoints.chat = `https://chat.${domain}`;
  } else {
    endpoints.api = domain.api;
    endpoints.upload = domain.upload;
    endpoints.chat = domain.chat;
  }
};

const initForBrowser = (domain: string | Endpoints) => {
  environment.isNode = false;
  environment.request = null;
  environment.cache = window["__SKILIKO_CACHE__"] || {};
  init(domain);
};

const initForNode = (request: Object, domain: string | Endpoints) => {
  environment.isNode = true;
  environment.request = request;
  environment.cache = {};
  init(domain);
};

export {
  initForBrowser,
  initForNode,
  endpoints,
  environment,
};
