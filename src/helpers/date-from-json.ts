const DateFromJson = (value): Date => {
  if (value === null || value === undefined || value === "") { return null; }
  if (parseInt(value, 10).toString() === value && value.length === 8) {
    // Case 20161225 => 25 dec. 2016
    // This format returned by google analytics for exemple
    const year = parseInt(value.substring(0, 4), 10);
    const month = parseInt(value.substring(4, 6), 10) - 1;
    const day = parseInt(value.substring(6), 10);
    return new Date(year, month, day);
  }
  return new Date(value);
};

export { DateFromJson };
