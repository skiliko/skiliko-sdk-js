import { DateFromJson } from "./date-from-json";

const DateToInput = (value): string => {
  if (!value) { return null; }
  try {
    return DateFromJson(value).toISOString().substring(0, 10);
  } catch (_) {
    return null;
  }
};

export { DateToInput };
