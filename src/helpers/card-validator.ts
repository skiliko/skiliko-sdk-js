const CardValidator = (number: string): boolean => {
  let nCheck = 0;
  let nDigit = 0;
  let bEven = false;

  const value = number.replace(/\D/g, "");

  for (let n = value.length - 1; n >= 0; n--) {
    nDigit = parseInt(value.charAt(n), 10);
    if (bEven) {
      nDigit *= 2;
      if (nDigit > 9) nDigit -= 9;
    }
    nCheck += nDigit;
    bEven = !bEven;
  }

  return nCheck % 10 === 0;
};

export { CardValidator };
