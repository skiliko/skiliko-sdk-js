const currencies = {
  EUR: "€",
  USD: "$",
  GBP: "£",
  CHF: "CHF",
};

const FormatPrice = (value: number, currency: string = "EUR"): string => {
  if (!value) { return "-"; }
  return [
    (value / 100).toFixed(2),
    currencies[currency],
  ].join("");
};

export { FormatPrice };
