const statusNames = [
  "queued",
  "ringing",
  "in-progress",
  "canceled",
  "completed",
  "busy",
  "failed",
  "no-answer",
];

const is = (status: string, list: string[]): boolean => {
  const index = statusNames.indexOf(status);
  const indexMax = list
  .map(e => statusNames.indexOf(e))
  .sort((a, b) => b - a)[0];
  return indexMax && indexMax >= index;
};

const CallWithStatus = (call: Object): Object => {

  const expertStatus = (call["callData"]["caller_states"] || []).map(e => e["state"]);
  const userStatus   = (call["callData"]["callee_states"] || []).map(e => e["state"]);

  const status = [
    { key: "callingExpert", value: is("ringing", expertStatus) },
    { key: "expertAnswering", value: is("in-progress", expertStatus) },
    { key: "callingUser", value: is("ringing", userStatus) },
    { key: "userAnswering", value: is("in-progress", userStatus) },
    { key: "finished", value: is("completed", userStatus) },
  ];

  return Object.assign({}, call, {
    status: (status.filter(e => e.value)
    .reverse()[0] || { key: "pending" }).key,
    statusList: status,
  });
};

export { CallWithStatus };
