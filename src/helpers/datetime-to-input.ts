import { DateFromJson } from "./date-from-json";

const DatetimeToInput = (datetime): string => {
  try {
    const date = DateFromJson(datetime);
    const offset = date.getTimezoneOffset();
    return (new Date(+date - offset * 60 * 1000))
    .toISOString()
    .substring(0, 16);
  } catch (e) {
    return null;
  }
};

export { DatetimeToInput };
