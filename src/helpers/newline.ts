const Newline = (text: string): string => {
  return text.replace(/(?:\r\n|\r|\n)/g, "<br />");
};

export { Newline };
