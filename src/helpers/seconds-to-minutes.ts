const SecondsToMinutes = (time: number): string => {
  let t = time;
  const minutes = Math.floor(t / 60);
  t -= minutes * 60;
  const seconds = parseInt((t % 60).toString(), 10);
  const secondsStr = seconds < 10 ? `0${seconds}` : seconds.toString();
  return `${minutes}m${secondsStr}s`;
};

export { SecondsToMinutes };
