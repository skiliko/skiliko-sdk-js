# Skiliko SDK

## Installation

```bash
npm i --save skiliko-sdk
```

## Initialize it

### Node

```javascript
const Skiliko = require('skiliko-sdk')
// request from your user's request to the server
Skiliko.initForNode(request, 'skiliko.com')
```

### Browser

```javascript
const Skiliko = require('skiliko-sdk')
Skiliko.initForBrowser('skiliko.com')
```

You can force a specific platform 
```javascript
Skiliko.Settings.set('token', {
  ...Skiliko.Settings.get('token'),
  platform_token: '__MY_PLATFORM_TOKEN__'
})
```

## Access Skiliko functions

You can have access to pretty much all APIs from Skiliko, Expert, Purchase, Session, User...

Here is the list of module you can have access

#### Card
- **create**: create a card for the user

#### Chat
- **getChat**: get the chat object
- **pendingMessages**: get the pending messages of a user

#### Country
- **codes**: list of country codes
- **all**: list of countries

#### Expert
- **all**: get all experts
- **get**: get a specific expert
- **todaySchedule**: know today schedule for an expert
- **nextSchedule**: know next schedule for an expert
- **available**: know if an expert is available
- **isRestricted**: know if an expert is restricted

#### Offer
- **all**: get all offers
- **get**: get a specific offer
- **promotions**: get all promotions
- **publicOffers**: get all public offers
- **prepaidMinutes**: get prepaid minutes offers

#### Purchase
- **all**: get all purchases of the user
- **get**: get a specific purchase for the user
- **create**: create a new purchase
- **save**: save a purchase
- **pay**: pay for a purchase
- **canAccept**: know if the user can accept a purchase
- **accept**: accept a purchase
- **canRefund**: know if the user can refund a purchase
- **refund**: refund a purchase
- **toReview**: get list of purchases to review
- **ratingNeeded**: know if a purchase needs reating

#### Rating
- **all**: get all ratings
- **create**: create a new rating

#### Session
- **init**: initialize user's session
- **signedIn**: know if the user is signed in
- **login**: login the user
- **logout**: logout the user
- **register**: register the user
- **unregister**: unregister the user
- **onSignedIn**: event when the user signed in
- **onSignedOut**: event when the user signed out

#### Settings
- **get**: get a setting
- **set**: set a setting 
- **remove**: remove a setting

#### Support
- **message**: send a message to the support

#### Timezone
- **detect**: detect the timezone
- **getSelectOptions**: returns label and value for timezone selection
- **scheduleToCurrentDates**: adapt a schedule to the current timezone

#### Upload
- **upload**: upload a file

#### User
- **current**: get the current user
- **save**: save the user